# A Simplified Model of the Brain

Disclaimer: this is fuzzy science, which means it's a simplification of a complex subject meant to be emotionally tangible so that the gut can aid in processing the information.

## First, let's keep it Simple

The brain is like a network of roads, where thoughts are cars.

Through travel and natural causes, roads weather until eventually a road crew comes by and fixes it up.

In the brain, most of these road crews work during sleep, and the firings of neurons being repaired creates dreams.

*Fuzzy science: Dreams are not fully understood, and obviously more complex than night working road crews.  That being said they do tend to be made up of strange old thoughts which have fallen into disrepair, and heavily used recent thoughts.

*Wild Speculation: If neuron repair does result in neuron firings, it could explain why we need sleep in the first place.  Having these misfirings amidst our day to day activities would likely create a similar confusion as that of dreams.

[citation](https://psychology.stackexchange.com/questions/20027/does-neural-repair-fire-off-neurons)

As with any city, the roads which get repaired the most slowly turn into main roads, highways, and freeways.

These highways and freeways become the basis of you.  If you're an honest person, these will be the some of the first parts people see of you.

And much like moving to big city and getting to know one part of town really well, different people will get to know different parts of you.

<!-- And much like moving to a city with bad roads, many an argument arises between two people who think human -->

## A little more Complex

So far this metaphor has been pretty straight-forward, but the next bit is a little less day to day life.  The thing about thought is that it travels less a car down a road and more like voices in a cave.

So if you can imagine a the city underground, where sound travels better down the bigger caves, and at each intersection stands a dog waiting to bark... that's sort of how neurons connect.

<!-- So if you can imagine the city where person stands at an intersection, and how well you can hear each-other is how big the roads between you are... that's sort of how neurons connect.  -->

When a neuron fires from an intersection, it branches off down all the roads around it.  Sometimes the message makes it all the way down the road, other times it just sort of fades away.

The messages it sends are neurotransmitters.  For this metaphor, the two most important messages will be called "bark" and "shush."  When a dog hears mostly barks, it barks too.  If it hears enough "shush"s, it does nothing.

Eventually all this barking and and shushing will result in something telling you to either move your hand off a hot stove, or keep it there for whatever reason you put it there in the first place.

## Leaving the binary behind

There are more than just two neurotransmitter messages though.  You can think of them as intonations, where a dog might bark or shush with happiness, arousal, purpose, confidence, or consolation in it's voice.

* bark - acetylcholine
A neurotransmitter used by neurons in the PNS and CNS in the control of functions ranging from muscle contraction and heart rate to digestion and memory.

* shush - GABA
inhibits the firing of neurons

* arousal - norepinephrine
A neurotransmitter involved in arousal, as well as in learning and mood regulation.

* purpose - serotonin
A neurotransmitter used by cells in parts of the brain involved in the regulation of sleep, mood and eating.

* happiness - dopamine
A neurotransmitter used in the parts of the brain involved in regulating movement and experiencing pleasure.

* confidence - glutamate
An excitatory neurotransmitter that helps strengthen synaptic connections between neurons.

* consolation - endorphin
bind to opiate receptors and moderate pain

## Now we're just getting silly

Now, if the underground city filled with dogs wasn't a complex enough metaphor, it get's a little bit deeper.  The roads between dogs are different for each type of message.  If some dog named Mr. Spot barks, the set of dogs which hear him are a completely different set than when he shushes.

Furthermore, Mr. Spot doesn't just send one type of a message at a time.  He might hear a bark and send all sorts of different messages down each of his connections.  He might send a bark to one dog, a shush to another, and silent happiness to a third, who then might send it on if some third dog barks at it.  It almost seems like complete chaos.

## How it isn't just chaos

So, one last question, why aren't all the dogs barking all the time?

Well, first of all, sometimes they do all bark together and when this happens it's usually called a seizure or a stroke.  But for the most part the brain self regulates towards some optimal level of activity.

It does this by making sure plenty of shushes are being sent out with the barks.  There's also a bit of a recharge period between a dog sending one message and the next.  It uses the confidence message to reward the right activities.  We also have some agency ourselves over this, most notably "taking a chill pill."  There's been an immense amount of trial and error involved with creating this system, and it will be many years before we find them all.

## In conclusion

So, there it is.  A rather silly metaphor of how the brain works, but a good place to start from none-the-less.

## A few more notes of interest:

Meditation is sort of like trying to get a bunch of dogs to quit barking  so much.


 <!-- to answer this question.  Why aren't all the dogs barking at once?

From this point on, things get highly speculative.

Well, first off, one theory is that sometimes they do, in particular when ones life is flashing before their eyes.  But for the most part, only a few major concepts ever seem to be in play -->
